FROM python:3.6-alpine

COPY requirements.txt .
RUN apk update && apk add git
RUN pip install -r requirements.txt

WORKDIR /app

COPY . .

CMD python3 api.py

class QueryAnalyzer:
    def __init__(self):
        self.OPERATORS = [
            '+', '-', '*', '/', '%', '&', '|', '^', '=', '<=', '>=', '>', '<', '<>', '+=', '-=', '*=', '/=', '%=', '&=',
            '^-=',
            '|*=', 'all', 'and', 'any', 'between', 'exists', 'in', 'like', 'not', 'or', 'some',
            'add', 'add constraint', 'alter', 'alter column', 'alter table', 'all', 'and', 'and', 'any', 'as', 'asc',
            'backup database', 'between', 'case', 'check', 'column', 'constraint', 'create', 'create database',
            'create index',
            'create or replace view', 'create table', 'create procedure', 'create unique index', 'create view',
            'database',
            'default', 'delete', 'desc', 'distinct', 'drop', 'drop column', 'drop constraint', 'drop database',
            'drop default',
            'drop index', 'drop table', 'drop view', 'exec', 'exists', 'foreign key', 'from', 'full outer join',
            'group by',
            'having', 'in', 'index', 'inner join', 'insert into', "insert into select", 'is null', 'is not null',
            'join',
            'left join', 'like', 'limit', 'not', 'not null', 'or', 'order by', 'outer join', 'primary key', 'procedure',
            'right join', 'rownum', 'select', 'select distinct', 'select into', 'select top', 'set', 'table', 'top',
            'truncate table', 'union', 'union all', 'unique', 'update', 'values', 'view', 'where'
        ]

    def halstead(self, query):
        query = query.lower()
        query = query.replace("(", " ")
        query = query.replace(")", " ")
        query = query.replace(",", " ")
        queries = query.split(";")
        difficulty_dict = {}
        operators = []
        operands = []
        for query in queries:
            if query != "" and query != " ":
                wordlist = query.split(" ")
                for word in wordlist:
                    if word in self.OPERATORS:
                        operators.append(word)
                    else:
                        operands.append(word)

                n1 = len(list(set(operators)))  # the number of distinct operators
                n2 = len(list(set(operands)))  # the number of distinct operands
                N2 = len(operands)  # the total number of operands
                difficulty = n1 / 2 * N2 / n2
                difficulty_dict[query] = difficulty
        return difficulty_dict

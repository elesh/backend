import logging
import random

from faker import Faker
from faker.providers import BaseProvider
import time

fake = Faker()

sleep = (0.05, 0.2)


def generate_logs(logger):
    try:
        while True:
            time.sleep(random.uniform(0.05, 0.2))
            logger.info(fake.log())
    except KeyboardInterrupt:
        return 0


requests = ["POST", "PUT", "GET", "DELETE", "PATCH"]

codes = [200, 201, 202, 203, 204, 205, 206, 300, 301, 302, 303, 304, 305, 307, 400, 401, 402, 403, 404, 405, 406, 407,
         408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 500, 501, 502, 503, 504, 505]

links = ["/api/v1/rooms/", "/api/v1/rooms/1/", "/api/v1/items/", "/api/v1/classes/", "/api/v1/courses/",
         "/static/rest_framework/css/bootstrap-tweaks.css", "/static/rest_framework/img/grid.png",
         "/static/rest_framework/js/ajax-form.js", "/static/rest_framework/css/default.css",
         "/static/rest_framework/js/csrf.js", "/static/rest_framework/js/jquery-3.3.1.min.js",
         "/static/rest_framework/js/bootstrap.min.js ", "/static/rest_framework/js/default.js ",
         "/static/rest_framework/css/prettify.css ", "/static/rest_framework/js/prettify-min.js ",
         "/static/rest_framework/css/bootstrap.min.css "]


class Server(BaseProvider):
    def request(self):
        return random.choice(requests)

    def code(self):
        return random.choice(codes)

    def link(self):
        if random.random() > 0.5:
            return random.choice(links)
        else:
            return f"/static/rest_framework/css/{'_'.join(fake.sentence()[:-1].split(' '))}.css"

    def log(self):
        return f'"{self.request()} {self.link()} HTTP/1.1" {self.code()} {random.randint(128, 8192)}'


fake.add_provider(Server)

logging.basicConfig(filename="testApp.log", level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] - %(message)s", datefmt="%d.%m.%Y %H:%M:%S")
logger = logging.getLogger()


generate_logs(logger)

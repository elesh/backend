from flask import Flask, request, jsonify
from flask_cors import CORS
from connection import ClickHouseManager
from QueryAnalyzer import QueryAnalyzer
import os

app = Flask(__name__)
CORS(app)

THRESHOLD = os.environ.get("THRESHOLD", 25)


@app.route('/')
def welcome_page():
    return "Hello page"


@app.route('/api/query', methods=['GET', 'POST'])
def select():
    if request.method == 'POST':
        data = request.get_json(silent=True)
        analyzer = QueryAnalyzer()
        queries_difficulty = analyzer.halstead(data['query'])
        for query_num in queries_difficulty.keys():
            if queries_difficulty[query_num] > THRESHOLD:
                return jsonify({'exception': f'Query "{query_num}" is difficult, please simplify'})
        client.create_connection()
        response = client.execute(data['query'], statistics=data['statistics'])
        client.close_connection()
        return jsonify(response)
    else:
        return "maybe select page"


if __name__ == '__main__':
    clickhouse_host = os.environ.get('CLICKHOUSE_HOST')
    client = ClickHouseManager(clickhouse_host)
    app.run(debug=True, host='0.0.0.0')
    # client = ClickHouseManager('localhost')
    # app.run()
